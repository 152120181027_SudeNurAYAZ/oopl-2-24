﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2LAB_24_TH1
{

    static class VigenèreCipher
    {
        public static string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public static string Encrypt(string s, string key)
        {

            int j = 0;
            StringBuilder ret = new StringBuilder(s.Length);
            for (int i = 0; i < s.Length; i++)
            {
                if (alphabet.Contains(s[i]))
                    ret.Append(alphabet[(alphabet.IndexOf(s[i]) + alphabet.IndexOf(key[j])) % alphabet.Length]);
                else
                    ret.Append(s[i]);
                j = (j + 1) % key.Length;
            }
            return ret.ToString();
        }

        public static string Decrypt(string s, string key)
        {


            //Decrypt the string
            int j = 0;
            StringBuilder ret = new StringBuilder(s.Length);
            for (int i = 0; i < s.Length; i++)
            {
                if (alphabet.Contains(s[i]))
                    ret.Append(alphabet[(alphabet.IndexOf(s[i]) - alphabet.IndexOf(key[j]) + alphabet.Length) % alphabet.Length]);
                else
                    ret.Append(s[i]);
                j = (j + 1) % key.Length;
            }
            return ret.ToString();
        }
    }
}
