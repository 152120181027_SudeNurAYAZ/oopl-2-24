﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2LAB_24_TH1
{
    public partial class AppSelect : Form
    {
        public AppSelect()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CALCULATOR c = new CALCULATOR();
            this.Hide();
            c.Show();
        }

        private void AppSelect_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Encryption frm = new Encryption();
            this.Hide();
            frm.Show();
        }
    }
}
