﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2LAB_24_TH1
{
    public partial class CALCULATOR : Form
    {
        double num1, num2, result;

        private void button2_Click(object sender, EventArgs e)
        {
            if (txt1.Text == "" || txt2.Text == "")
                MessageBox.Show("NUM1 VE NUM2 DEĞERLERİ BOŞ OLAMAZ!");

            else
            {
                button1.BackColor = Color.White;
                button3.BackColor = Color.White;
                button2.BackColor = Color.Yellow;
                button4.BackColor = Color.White;

                num1 = Convert.ToDouble(txt1.Text);
                num2 = Convert.ToDouble(txt2.Text);
                result = num1 - num2;
                label4.Text = Convert.ToString(result);
                if (result <= 0)
                    label4.BackColor = Color.Red;
                else
                    label4.BackColor = Color.Green;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (txt1.Text == "" || txt2.Text == "")
                MessageBox.Show("NUM1 VE NUM2 DEĞERLERİ BOŞ OLAMAZ!");

            else
            {
                button1.BackColor = Color.White;
                button2.BackColor = Color.White;
                button3.BackColor = Color.Yellow;
                button4.BackColor = Color.White;

                num1 = Convert.ToDouble(txt1.Text);
                num2 = Convert.ToDouble(txt2.Text);
                result = num1 * num2;
                label4.Text = Convert.ToString(result);
                if (result <= 0)
                    label4.BackColor = Color.Red;
                else
                    label4.BackColor = Color.Green;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (txt1.Text == "" || txt2.Text == "")
                MessageBox.Show("NUM1 VE NUM2 DEĞERLERİ BOŞ OLAMAZ!");

            else
            {
                num1 = Convert.ToDouble(txt1.Text);
                num2 = Convert.ToDouble(txt2.Text);
            }

            if (num2 == 0)
                MessageBox.Show(" 0'a bölemezsiniz.");
            else
            {
                button1.BackColor = Color.White;
                button2.BackColor = Color.White;
                button4.BackColor = Color.Yellow;
                button3.BackColor = Color.White;
                num1 = Convert.ToDouble(txt1.Text);
                num2 = Convert.ToDouble(txt2.Text);
                result = num1 / num2;
                label4.Text = Convert.ToString(result);
                if (result <= 0)
                    label4.BackColor = Color.Red;
                else
                    label4.BackColor = Color.Green;
            }
        }

        private void CALCULATOR_Load(object sender, EventArgs e)
        {

        }

        private void button1_MouseMove(object sender, MouseEventArgs e)
        {
            button1.ForeColor = Color.Red;
        }

        private void button3_MouseMove(object sender, MouseEventArgs e)
        {
            button3.ForeColor = Color.Red;
        }

        private void button2_MouseMove(object sender, MouseEventArgs e)
        {
            button2.ForeColor = Color.Red;
        }

        private void button4_MouseMove(object sender, MouseEventArgs e)
        {
            button4.ForeColor = Color.Red;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.ForeColor = Color.Black;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.ForeColor = Color.Black;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.ForeColor = Color.Black;
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            button4.ForeColor = Color.Black;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            User frm = new User();
            this.Hide();
            frm.Show();
        }

        public CALCULATOR()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txt1.Text == "" || txt2.Text == "")
                MessageBox.Show("NUM1 VE NUM2 DEĞERLERİ BOŞ OLAMAZ!");

            else
            {
                button3.BackColor = Color.White;
                button2.BackColor = Color.White;
                button1.BackColor = Color.Yellow;
                button4.BackColor = Color.White;
                num1 = Convert.ToDouble(txt1.Text);
                num2 = Convert.ToDouble(txt2.Text);
                result = num1 + num2;
                label4.Text = Convert.ToString(result);
                if (result <= 0)
                    label4.BackColor = Color.Red;
                else
                    label4.BackColor = Color.Green;
            }
        }
    }
}
