﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2LAB_24_TH1
{


    public partial class User : Form
    {

       // string[] names = { "betül24", "kübra24", "cihan24", "sude24" };
       // string[] pass = { "12345be", "12345küb", "12345ci", "12345su" };
        public User()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            UserDatabase userDatabase = UserDatabase.Instance;
            textBox1.Text = userDatabase.username;
            textBox2.Text = userDatabase.password;

            if (textBox1.Text != userDatabase.username || textBox2.Text != userDatabase.password)
                {

                    label4.Text = "Hatalı veya eksik giriş yaptınız. Lütfen tekrar deneyiniz.";
                }
                else
                {
                    AppSelect app = new AppSelect();
                    this.Hide();
                    app.Show();
                    

                }
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            
                if (checkBox1.Checked)
                {
                    textBox2.PasswordChar = '\0';
                }
                else
                {
                    textBox2.PasswordChar = '*';
                }
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == false)
                textBox2.PasswordChar = '*';
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Sign frm = new Sign();
            this.Hide();
            frm.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
