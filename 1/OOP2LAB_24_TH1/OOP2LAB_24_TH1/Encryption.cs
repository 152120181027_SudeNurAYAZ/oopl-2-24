﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2LAB_24_TH1
{
    public partial class Encryption : Form
    {
        public Encryption()
        {
            InitializeComponent();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            int key = 0;
            if (textBox2.Text != "")
                key = Int32.Parse(textBox2.Text);



            if (radioButton3.Checked)
                label6.Text = CeaserCipher.Encrypt_C(textBox1.Text, key);
            else
                label6.Text = CeaserCipher.Decrypt_C(textBox1.Text, key);
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            int key = 0;
            if (textBox2.Text != "")
                key = Int32.Parse(textBox2.Text);



            if (radioButton3.Checked)
                label6.Text = VigenèreCipher.Encrypt(textBox1.Text, textBox3.Text);
            else
                label6.Text = VigenèreCipher.Decrypt(textBox1.Text, textBox3.Text);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            int key = 0;
            if (textBox2.Text != "")
                key = Int32.Parse(textBox2.Text);
            if (radioButton2.Checked)
                if (radioButton3.Checked)
                    label6.Text = VigenèreCipher.Encrypt(textBox1.Text, textBox3.Text);
                else
                    label6.Text = VigenèreCipher.Decrypt(textBox1.Text, textBox3.Text);
            else if (radioButton1.Checked)
                if (radioButton3.Checked)
                    label6.Text = CeaserCipher.Encrypt_C(textBox1.Text, key);
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            int key = 0;
            if (textBox2.Text != "")
                key = Int32.Parse(textBox2.Text);
            if (radioButton2.Checked)
                label6.Text = VigenèreCipher.Encrypt(textBox1.Text, textBox3.Text);
            else if (radioButton1.Checked)
                label6.Text = CeaserCipher.Encrypt_C(textBox1.Text, key);
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            int key = 0;
            if (textBox2.Text != "")
                key = Int32.Parse(textBox2.Text);
            if (radioButton2.Checked)
                label6.Text = VigenèreCipher.Decrypt(textBox1.Text, textBox3.Text);
            else if (radioButton1.Checked)
                label6.Text = CeaserCipher.Decrypt_C(textBox1.Text, key);
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {


            string UserString = textBox4.Text;
            int key = Int32.Parse(textBox2.Text);
            label4.Text = CeaserCipher.Encrypt_C(UserString, key);
            //alphabet = textBox4.Text;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            int key = 0;
            if (textBox2.Text != "")
                key = Int32.Parse(textBox2.Text);
            if (radioButton2.Checked)
                if (radioButton3.Checked)
                    label6.Text = VigenèreCipher.Encrypt(textBox1.Text, textBox3.Text);
                else
                    label6.Text = VigenèreCipher.Decrypt(textBox1.Text, textBox3.Text);
            else if (radioButton1.Checked)
                if (radioButton3.Checked)
                    label6.Text = CeaserCipher.Encrypt_C(textBox1.Text, key);
        }
    }
}