﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OOP2LAB_24_TH1
{
    class UserDatabase
    {
        private UserDatabase()
        {

        }

        private static UserDatabase instance = null;

        public static UserDatabase Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserDatabase();
                }
                return instance;
            }
        }

        public string username { get; set; }
        public string password { get; set; }

        public string GetMd5(string sifreleme)
        {

            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

            byte[] dizi = Encoding.UTF8.GetBytes(sifreleme);

            dizi = md5.ComputeHash(dizi);

            StringBuilder sb = new StringBuilder();


            foreach (byte ba in dizi)
            {

                sb.Append(ba.ToString("x2").ToLower());
            }

            return sb.ToString();
        }
    }

}

