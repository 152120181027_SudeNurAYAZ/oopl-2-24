﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Collections;

namespace OOP2LAB_24_TH1
{
    public partial class Sign : Form
    {
        public Sign()
        {
            InitializeComponent();
        }

        int sayac = 0;
        ArrayList users = new ArrayList();
        ArrayList pswrds = new ArrayList();

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Save_Click(object sender, EventArgs e)
        {

            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "")
            {
                label5.Text = "Eksik giriş yaptınız.";
            }
            else
            {

                if (users.Contains(textBox1.Text))
                {
                    label5.Text = "Bu kullanıcı adı zaten kullanılmış.";

                }
                else
                {
                    if (textBox2.Text == textBox3.Text)
                    {

                        //Database static classını kullanarakta kullanıcıları kaydedebiliriz
                        /*
                            Database.username = textBox1.Text;
                            Database.password = Database.GetMd5(textBox2.Text);
                             */

                        UserDatabase userDatabase = UserDatabase.Instance;
                        userDatabase.username = textBox1.Text;
                        userDatabase.password = userDatabase.GetMd5(textBox2.Text);

                        users.Add(textBox1.Text);
                        pswrds.Add(textBox2.Text);
                        sayac++;

                        User frm = new User();
                        frm.Show();
                        this.Close();
                    }
                    else
                    {
                        label5.Text = "Girdiğiniz şifreler eşleşmemektedir.";
                    }
                }
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void Sign_Load(object sender, EventArgs e)
        {

            textBox3.PasswordChar = '*';
            textBox2.PasswordChar = '*';
        }
    }
}
