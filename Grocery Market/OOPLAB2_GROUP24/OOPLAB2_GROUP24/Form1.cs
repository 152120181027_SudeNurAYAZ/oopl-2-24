﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace OOPLAB2_GROUP24
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        BindingList<Product> products;
        string image_path;
        private void button_add_Click(object sender, EventArgs e)
        {
            int selectedIndex = listBox_products.SelectedIndex;
            //Product p = listBox_products.SelectedItem as Product;

            if (selectedIndex == -1 || textBox_productname.Text == "" || textBox_productdescription.Text == "" || textBox_productprice.Text == "" || image_path == null || double.Parse(textBox_productprice.Text) ==null)
            {

                MessageBox.Show("Please Check the Values ​​You Entered. Select Image, Enter Number For Price ");
            }
            else
            {
                Product p = new Product(textBox_productname.Text, textBox_productdescription.Text, double.Parse(textBox_productprice.Text), image_path);

                products.Add(p);
            }



        }

        private void button_update_Click(object sender, EventArgs e)
        {
            int selectedIndex = listBox_products.SelectedIndex;


            if (selectedIndex == -1 || textBox_productname.Text == "" || textBox_productdescription.Text == "" || image_path == null|| textBox_productprice.Text == "" || double.Parse(textBox_productprice.Text) == null)
            {

                MessageBox.Show("Please Check the Values ​​You Entered. You Can Try to Choose a Photo For Update");
            }
            else
            {
               
             
                Product p = new Product(textBox_productname.Text, textBox_productdescription.Text, double.Parse(textBox_productprice.Text), image_path);

                p.name = textBox_productname.Text;
                p.price = double.Parse(textBox_productprice.Text);
                p.description = textBox_productdescription.Text;

                    if (products.Count > 0)
                    {

                        pictureBox1.Image = p.photo;
                        fiyatValue.Text = p.price.ToString();
                        descValue.Text = p.description;
                    }


                    products.Add(p);
                    products.RemoveAt(selectedIndex);

                   

                
                   
              


            }


        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            int selectedIndex = listBox_products.SelectedIndex;

           
            if (selectedIndex != -1)
            {
                products.RemoveAt(selectedIndex);
            }
            else
            {
                MessageBox.Show("Please Try Again. Product not selected or no product ");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void listBox_products_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (products.Count > 0)
            {
                Product p = listBox_products.SelectedItem as Product;
                pictureBox1.Image = p.photo;
                fiyatValue.Text = p.price.ToString();
                descValue.Text = p.description;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string imagename = openFileDialog1.FileName;
            image_path = Path.Combine(Environment.CurrentDirectory, @"Images\", imagename);



        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            products = new BindingList<Product>();
            listBox_products.DataSource = products;
            listBox_products.DisplayMember = "name";
            listBox_products.ValueMember = "name";


            Product apple = new Product("Apple", "Fruit", 2.5, Path.Combine(Environment.CurrentDirectory, @"Images\", "apple.jpeg"));
            products.Add(apple);

            Product orange = new Product("Orange", "Fruit", 1.5, Path.Combine(Environment.CurrentDirectory, @"Images\", "orange.jpg"));
            products.Add(orange);

            Product pineapple = new Product("Pineapple", "Fruit", 2.55, Path.Combine(Environment.CurrentDirectory, @"Images\", "pineapple.jpeg"));
            products.Add(pineapple);

            Product tomato = new Product("Tomato", "Fruit", 3.55, Path.Combine(Environment.CurrentDirectory, @"Images\", "domates.jpeg"));
            products.Add(tomato);

            Product strawberry = new Product("Strawberry", "Fruit", 11.5, Path.Combine(Environment.CurrentDirectory, @"Images\", "çilek.jpg"));
            products.Add(strawberry);

            Product cucumber = new Product("Cucumber", "Vegetable", 5.5, Path.Combine(Environment.CurrentDirectory, @"Images\", "salatalık.jpg"));
            products.Add(cucumber);

            Product lettuce = new Product("Lettuce", "Vegetable", 12.5, Path.Combine(Environment.CurrentDirectory, @"Images\", "marul.jpeg"));
            products.Add(lettuce);

            Product pear = new Product("Pear", "Fruit", 8.5, Path.Combine(Environment.CurrentDirectory, @"Images\", "armut.jpeg"));
            products.Add(pear);

            Product avocado = new Product("Avocado", "Fruit", 14.5, Path.Combine(Environment.CurrentDirectory, @"Images\", "avakado.jpeg"));
            products.Add(avocado);

            Product redcabbage  = new Product("Red cabbage", "Vegetable", 0.5, Path.Combine(Environment.CurrentDirectory, @"Images\", "kırmızılahana.jpeg"));
            products.Add(redcabbage);
        }

       

        private void button2_Click(object sender, EventArgs e)
        {
            int selectedIndex = listBox_products.SelectedIndex;

            if(selectedIndex != -1)
            {
                Product p = listBox_products.SelectedItem as Product;
                textBox_productname.Text = p.name;
                textBox_productprice.Text = p.price.ToString();
                textBox_productdescription.Text = p.description;
                pictureBox1.Image = p.photo;
            }
            else
            {
                MessageBox.Show("Please Try Again. Product not selected or no product ");
            }

          
        }
    }
}