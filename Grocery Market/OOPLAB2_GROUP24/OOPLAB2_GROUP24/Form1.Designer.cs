﻿namespace OOPLAB2_GROUP24
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_add = new System.Windows.Forms.Button();
            this.button_update = new System.Windows.Forms.Button();
            this.button_delete = new System.Windows.Forms.Button();
            this.label_welcome = new System.Windows.Forms.Label();
            this.listBox_products = new System.Windows.Forms.ListBox();
            this.label_products = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label_productprice = new System.Windows.Forms.Label();
            this.label_productdescription = new System.Windows.Forms.Label();
            this.textBox_productprice = new System.Windows.Forms.TextBox();
            this.textBox_productdescription = new System.Windows.Forms.TextBox();
            this.textBox_productname = new System.Windows.Forms.TextBox();
            this.label_productname = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.fiyatLabel = new System.Windows.Forms.Label();
            this.fiyatValue = new System.Windows.Forms.Label();
            this.descValue = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.updateButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button_add
            // 
            this.button_add.BackColor = System.Drawing.Color.LightGray;
            this.button_add.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button_add.ForeColor = System.Drawing.Color.Black;
            this.button_add.Location = new System.Drawing.Point(510, 310);
            this.button_add.Margin = new System.Windows.Forms.Padding(2);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(71, 37);
            this.button_add.TabIndex = 0;
            this.button_add.Text = "Add";
            this.button_add.UseVisualStyleBackColor = false;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // button_update
            // 
            this.button_update.BackColor = System.Drawing.Color.LightGray;
            this.button_update.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button_update.ForeColor = System.Drawing.Color.Black;
            this.button_update.Location = new System.Drawing.Point(407, 310);
            this.button_update.Margin = new System.Windows.Forms.Padding(2);
            this.button_update.Name = "button_update";
            this.button_update.Size = new System.Drawing.Size(80, 37);
            this.button_update.TabIndex = 1;
            this.button_update.Text = "Update";
            this.button_update.UseVisualStyleBackColor = false;
            this.button_update.Click += new System.EventHandler(this.button_update_Click);
            // 
            // button_delete
            // 
            this.button_delete.BackColor = System.Drawing.Color.LightGray;
            this.button_delete.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button_delete.ForeColor = System.Drawing.Color.Black;
            this.button_delete.Location = new System.Drawing.Point(317, 310);
            this.button_delete.Margin = new System.Windows.Forms.Padding(2);
            this.button_delete.Name = "button_delete";
            this.button_delete.Size = new System.Drawing.Size(70, 37);
            this.button_delete.TabIndex = 2;
            this.button_delete.Text = "Delete";
            this.button_delete.UseVisualStyleBackColor = false;
            this.button_delete.Click += new System.EventHandler(this.button_delete_Click);
            // 
            // label_welcome
            // 
            this.label_welcome.AutoSize = true;
            this.label_welcome.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_welcome.ForeColor = System.Drawing.Color.AliceBlue;
            this.label_welcome.Location = new System.Drawing.Point(79, 21);
            this.label_welcome.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_welcome.Name = "label_welcome";
            this.label_welcome.Size = new System.Drawing.Size(450, 32);
            this.label_welcome.TabIndex = 3;
            this.label_welcome.Text = "WELCOME TO GROCERY STORE";
            // 
            // listBox_products
            // 
            this.listBox_products.BackColor = System.Drawing.Color.LightGray;
            this.listBox_products.FormattingEnabled = true;
            this.listBox_products.Location = new System.Drawing.Point(9, 102);
            this.listBox_products.Margin = new System.Windows.Forms.Padding(2);
            this.listBox_products.Name = "listBox_products";
            this.listBox_products.Size = new System.Drawing.Size(103, 147);
            this.listBox_products.TabIndex = 4;
            this.listBox_products.SelectedIndexChanged += new System.EventHandler(this.listBox_products_SelectedIndexChanged);
            // 
            // label_products
            // 
            this.label_products.AutoSize = true;
            this.label_products.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_products.ForeColor = System.Drawing.Color.White;
            this.label_products.Location = new System.Drawing.Point(19, 71);
            this.label_products.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_products.Name = "label_products";
            this.label_products.Size = new System.Drawing.Size(88, 21);
            this.label_products.TabIndex = 5;
            this.label_products.Text = "Products";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(125, 75);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(174, 199);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label_productprice
            // 
            this.label_productprice.AutoSize = true;
            this.label_productprice.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_productprice.ForeColor = System.Drawing.Color.White;
            this.label_productprice.Location = new System.Drawing.Point(314, 163);
            this.label_productprice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_productprice.Name = "label_productprice";
            this.label_productprice.Size = new System.Drawing.Size(105, 18);
            this.label_productprice.TabIndex = 25;
            this.label_productprice.Text = "Product Price";
            // 
            // label_productdescription
            // 
            this.label_productdescription.AutoSize = true;
            this.label_productdescription.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_productdescription.ForeColor = System.Drawing.Color.White;
            this.label_productdescription.Location = new System.Drawing.Point(314, 122);
            this.label_productdescription.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_productdescription.Name = "label_productdescription";
            this.label_productdescription.Size = new System.Drawing.Size(150, 18);
            this.label_productdescription.TabIndex = 24;
            this.label_productdescription.Text = "Product Description";
            // 
            // textBox_productprice
            // 
            this.textBox_productprice.Location = new System.Drawing.Point(480, 156);
            this.textBox_productprice.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_productprice.Multiline = true;
            this.textBox_productprice.Name = "textBox_productprice";
            this.textBox_productprice.Size = new System.Drawing.Size(102, 29);
            this.textBox_productprice.TabIndex = 23;
            // 
            // textBox_productdescription
            // 
            this.textBox_productdescription.Location = new System.Drawing.Point(480, 115);
            this.textBox_productdescription.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_productdescription.Multiline = true;
            this.textBox_productdescription.Name = "textBox_productdescription";
            this.textBox_productdescription.Size = new System.Drawing.Size(102, 29);
            this.textBox_productdescription.TabIndex = 22;
            // 
            // textBox_productname
            // 
            this.textBox_productname.Location = new System.Drawing.Point(480, 75);
            this.textBox_productname.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_productname.Multiline = true;
            this.textBox_productname.Name = "textBox_productname";
            this.textBox_productname.Size = new System.Drawing.Size(102, 29);
            this.textBox_productname.TabIndex = 21;
            // 
            // label_productname
            // 
            this.label_productname.AutoSize = true;
            this.label_productname.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_productname.ForeColor = System.Drawing.Color.White;
            this.label_productname.Location = new System.Drawing.Point(314, 82);
            this.label_productname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_productname.Name = "label_productname";
            this.label_productname.Size = new System.Drawing.Size(108, 18);
            this.label_productname.TabIndex = 20;
            this.label_productname.Text = "Product Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(314, 202);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 18);
            this.label1.TabIndex = 26;
            this.label1.Text = "Product Image";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightGray;
            this.button1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(480, 201);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 29);
            this.button1.TabIndex = 27;
            this.button1.Text = "Select Image";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // fiyatLabel
            // 
            this.fiyatLabel.AutoSize = true;
            this.fiyatLabel.Location = new System.Drawing.Point(9, 264);
            this.fiyatLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.fiyatLabel.Name = "fiyatLabel";
            this.fiyatLabel.Size = new System.Drawing.Size(31, 13);
            this.fiyatLabel.TabIndex = 28;
            this.fiyatLabel.Text = "Price";
            // 
            // fiyatValue
            // 
            this.fiyatValue.AutoSize = true;
            this.fiyatValue.Location = new System.Drawing.Point(42, 264);
            this.fiyatValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.fiyatValue.Name = "fiyatValue";
            this.fiyatValue.Size = new System.Drawing.Size(0, 13);
            this.fiyatValue.TabIndex = 29;
            // 
            // descValue
            // 
            this.descValue.AutoSize = true;
            this.descValue.Location = new System.Drawing.Point(88, 292);
            this.descValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.descValue.Name = "descValue";
            this.descValue.Size = new System.Drawing.Size(0, 13);
            this.descValue.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 292);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Description";
            // 
            // updateButton
            // 
            this.updateButton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.updateButton.Location = new System.Drawing.Point(407, 249);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(80, 56);
            this.updateButton.TabIndex = 33;
            this.updateButton.Text = "Show Product Details";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkRed;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.descValue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fiyatValue);
            this.Controls.Add(this.fiyatLabel);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_productprice);
            this.Controls.Add(this.label_productdescription);
            this.Controls.Add(this.textBox_productprice);
            this.Controls.Add(this.textBox_productdescription);
            this.Controls.Add(this.textBox_productname);
            this.Controls.Add(this.label_productname);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label_products);
            this.Controls.Add(this.listBox_products);
            this.Controls.Add(this.label_welcome);
            this.Controls.Add(this.button_delete);
            this.Controls.Add(this.button_update);
            this.Controls.Add(this.button_add);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Grocery Market 24";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.Button button_update;
        private System.Windows.Forms.Button button_delete;
        private System.Windows.Forms.Label label_welcome;
        private System.Windows.Forms.ListBox listBox_products;
        private System.Windows.Forms.Label label_products;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label_productprice;
        private System.Windows.Forms.Label label_productdescription;
        private System.Windows.Forms.TextBox textBox_productprice;
        private System.Windows.Forms.TextBox textBox_productdescription;
        private System.Windows.Forms.TextBox textBox_productname;
        private System.Windows.Forms.Label label_productname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label fiyatLabel;
        private System.Windows.Forms.Label fiyatValue;
        private System.Windows.Forms.Label descValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button updateButton;
    }
}

