﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace OOPLAB2_GROUP24
{
    class Product
    {

        public string name { get; set; }
        public Image photo { get; set; }
        public string description { get; set; }
        public double price { get; set; }
            
        public Product(string n, string desc, double prc, string photoPath)
        {
            name = n;
            description = desc;
            price = prc;
       
              photo = Image.FromFile(photoPath);
       
        }

    }
}
